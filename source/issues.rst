Issue Tracking
==============

Each GNOME module has an issue tracker in its GitLab project. This is where problems with GNOME's software can be reported.

General guidelines
------------------

GNOME's issue tracker is a vital development tool whose role is to track issues and tasks. Everyone who uses it has a responsibility to make sure that it continues to work effectively. This means:

* taking time and care when creating reports - see the :doc:`issue reporting guidelines </issues/reporting>`
* not using the issue tracker frivolously
* avoiding idle discussion, "me too" comments, and repetition of points that have already been made
* staying on topic
* ensuring that comments are succinct and not overly verbose
* updating your own issues when their status changes (such as no longer being relevant or being resolved)
* being polite and empathetic, and following the `code of conduct <https://conduct.gnome.org/>`_

Contributing to issue review
----------------------------

Reviewing GNOME's issue reports is an extremely valuable activity. These pages include guidelines and advice on how to do this, including :ref:`advice for contributors who want to help <contributing issue review>`.

.. toctree::
   :hidden:

   issues/reporting
   issues/stack-traces
   issues/management
   issues/review
   issues/stock-responses
   issues/template

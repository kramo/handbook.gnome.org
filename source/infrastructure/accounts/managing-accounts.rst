Managing Accounts
=================

This page provides information on how to manage an existing :doc:`GNOME account </infrastructure/accounts>`.

Change email address
--------------------

The email address associated with your GNOME account is used for your gnome.org email alias, as well as notifications related to the account. To change the account email address:

* Login to GitLab.
* In your `GitLab profile email settings <https://gitlab.gnome.org/-/profile/emails>`_, set the secondary email address to the one you want to use for your GNOME account.
* `Open an account request issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=account-request>`_.
* Replace ``${action_name}`` with ``update_email``.
* Create the issue.

Change password
---------------

To change your GNOME account password, use the page at `password.gnome.org <https://password.gnome.org/>`_.

Change your name or other personal details
------------------------------------------

The personal details associated with your GNOME account can be changed at `idm01.gnome.org <https://idm01.gnome.org>`_.

.. _master-access:

Request master.gnome.org access
-------------------------------

Existing maintainers can request master.gnome.org commit access, for pushing new releases. Access will only be granted if your details are in the relevant module's DOAP file.

To request master.gnome.org access:

* Login to GitLab and `open an account request issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=account-request>`_.
* Replace ``${action_name}`` with ``maint_access``.
* Create the issue.

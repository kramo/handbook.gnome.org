Issue Reporting
===============

Problems with GNOME's software can be reported using its issue tracker. Issue reports should be created in accordance with the following guidelines.

Preparation
-----------

Before you create a report, it is important to do some preliminary research.

* Check that the GNOME version you are reporting an issue about is supported: this should be either the most recent stable release or the previous stable release before that. You can also file issues about the most recent development release.
   * If your GNOME version is no longer supported, you can try to reproduce the issue in a newer version. Virtual machines can be a useful way to do this.
* Confirm that the issue is a GNOME issue. If you're not sure about this, check with your distro using a support channel or distro issue tracker.
* Check whether the issue has already been reported, by searching for relevant terms in the issue tracker of the relevant GNOME component. Make sure to look for both open and closed issues.

Issue reporting steps
---------------------

To report an issue, you will need to use GNOME's Gitlab instance:

* Open the `GNOME group in Gitlab <https://gitlab.gnome.org/GNOME>`_.
* Find the software module that relates to your issue by searching or browsing the list.
* In the sidebar, select **Issues**.
* Click the blue **New issue** button at the top of the page.
* If you are not signed in already, you will be prompted to do so.

Writing the report
------------------

High-quality reports are easier to process, diagnose and resolve. Therefore, please take time and care when creating issues. This includes the following:

* Avoid broad and ambiguous titles. Instead, give your issue a descriptive title, and ensure that it includes specifics.
   * Example: instead of writing: "Online accounts is not working", write "Browser window is blank when adding an online account".
* The body of the issue should include a detailed description of what happened:
   * Describe what happened as a series of steps, including the actions you took and what happened as a result. Tell it as a story from your personal perspective: what did you do and what was the result?
   * State your expectations: what did you think should happen?
   * How often does the issue occur? Does it happen every time, or only sometimes? Does it only happen under particular conditions?
* Issue reports should also include:
   * The version of the component where the problem occurred.
   * The distribution and its version.
   * A screenshot or a screen recording of the issue. (When taking these, please ensure that your system is in US English.)
   * If the report is of a crash, it should ideally include a :doc:`stack trace </issues/stack-traces>` (without this, it will be hard to fix).

Feature requests
----------------

Most issues describe problems with the software in question (these are often referred to as "bugs"). However, in some cases a reporter may want to use an issue to request a feature or functionality which is currently missing.

Guidelines for requesting new features:

* Some GNOME modules require feature requests to be made in a forum other than the issue tracker. It is recommended to check the module's Gitlab page and issue template to see whether this is the case.
* As with other types of issue reports, when requesting a new feature it is important to provide a detailed description of the request. Why do you personally want the feature? In what way are existing solutions unsatisfactory? When answering these questions, include any relevant details about the conditions in which you are working, or practical factors which are influencing your requirements.
* Be aware that they can require significant time and effort from developers, and that feature requests need to be accommodated alongside existing development plans. Consultation with the design team is often also required.

General guidelines
------------------

* Issue reports should be factual and descriptive, and should stick to the technical and design issues at hand. Avoid emotive language or judgments, and do not express upset, anger, outrage or disappointment. All interactions on the issue tracker should follow the `Code of Conduct <https://conduct.gnome.org/>`_.
* Only report one problem in an issue. If you have more than one problem, create multiple issue reports.
* Avoid proposing solutions without giving background information. There should always be a description of the issue that the proposal is trying to resolve, or the motivations for a change. When doing this, describe your personal experience rather than giving general rationales.
* Be realistic in your expectations. The GNOME project receives more issue reports than can be practically resolved, so it's possible that your issue will not be fixed. In some cases it can take a long time for a developer to respond.

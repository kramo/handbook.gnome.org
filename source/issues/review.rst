Issue Review
============

Issue review is the activity of going through a set of issues one by one, checking them and making any necessary changes. In doing so, the overall quality of the issue set is improved. Typically, issue review is done by maintainers and established contributors, but new contributors can also help out with this valuable activity (see :ref:`contributing <contributing issue review>`).

When doing issue review, it is usually best to focus on one or two GNOME modules. This allows you to become familiar with the issues in the tracker, which makes the process a lot easier.

Finding issues to review
------------------------

The first step in doing issue review is deciding which issues you will look at. This will typically be:

* Newly reported issues which have not yet been reviewed (called "triage").
* Sets of issues which are likely to have quality issues, including:
   * a module's oldest issues
   * issues which haven't seen activity in over a year
   * issues which are labelled with **Needs Information** or **Needs Design**
* Issues about a particular feature or subcomponent.

GitLab's issue filters and sort functionality is an effective way to do this.

Review Process
--------------

When reviewing an issue, check it for each of the following:

1. Quality
~~~~~~~~~~

First, check that the issue makes sense and contains the necessary information:

* Can you easily understand the issue that is being described?
* Is the title accurate, specific, and easy to understand?
* Does the report describe one issue or multiple issues?
* Are any of the following missing?
   * Module or GNOME version
   * Distribution name and version
   * For UI issues: screenshot or screen recording (in US English)
   * For crashes: :doc:`a stack trace </issues/stack-traces>`

Possible actions:

* If it isn't clear what issue is being described, ask the reporter to clarify and add the **Needs Information** label.
* If the title isn't clear or specific, rewrite it.
* If the report describes multiple issues, try to disentangle them by either:
   * Selecting one of the issues, and adding a comment to say 1) that this will be the sole focus of the report, and 2) ask that other issues be reported separately.
   * Adding the **Needs Information** label and asking the reporter to clarify which single issue the report is about.
* If any information is missing, politely request it and add the **Needs Information** label.
* If essential information has been requested and is still missing after 6 weeks, add the **Not Actionable** label and close the issue.

2. Not GNOME Issues
~~~~~~~~~~~~~~~~~~~

If the issue does not belong to a GNOME module: add the **Not GNOME** label and close the issue. If possible, include advice about where the issue should be reported.

3. Desirability
~~~~~~~~~~~~~~~

If the issue describes an enhancement or feature request:

* If the module doesn't accept new feature requests in the issue tracker: politely close the issue and point the reporter in the direction of the appropriate forum.
* If the request is for a change that isn't wanted, add the **Out of Scope** label and close. (This decision can typically only be made by maintainers, sometimes in conjunction with the design team.)

If the behavior being reported is intentional and desirable (in other words, the bug isn't actually a bug): add the **Expected Behavior** label and close.

4. Component
~~~~~~~~~~~~

If the issue hasn't been reported against the correct component:

* If you know the correct component and it is part of the GNOME group, move it there.
* If you don't know the correct component, close the issue with a comment explaining why.

5. Duplicates
~~~~~~~~~~~~~

Do a quick search to see if there are obvious duplicates. If you find a duplicate, close one of the duplicate issues:

* Choose which one to close: typically this should be the newer issue, but it can be the older issue if the more recent one has more recent or useful discussion.
* Enter ``/duplicate #1`` into the comment field, using the number of the duplicate issue that will remain open.

6. Recency
~~~~~~~~~~

If an issue has not seen activity in over a year:

* Add the **Needs Information** label and request for the issue to be reproduced with a supported GNOME version.
* If the issue sees no activity for a further six weeks, it can be closed.

If an issue has been labelled as **Needs Design** for more than a year:

* Ask for confirmation that it is still wanted.
* If there is no confirmation after 6 weeks, it can be closed.

7. Labelling
~~~~~~~~~~~~

Open issues should be classified using the standard GNOME labels. This includes:

* Issue type: bug, cleanup, crash, enhancement, feature, performance, regression, security.
* Contribution type: help wanted, newcomers.

.. _contributing issue review:

Contributing
------------

Some issue review steps can only be performed by experienced contributors, and permissions for certain actions in the issue tracker are restricted. However, many issue review tasks can be done by anyone. This includes:

* Finding duplicate issues.
* Identifying issues that are filed against an incorrect component.
* Confirming whether you can or can't reproduce newly created issues on your system.
* Confirming whether old issues can or can't be reproduced using a recent GNOME version.

You can do each of these activities without needing special permission. In each case, you can comment on the issue with your findings. The maintainers of the component will see your comment and can take appropriate action. They will also thank you for it!

Another way to get started with issue management activities is to speak directly to a module's maintainer. Decide which module you are interested in, familiarize yourself with its issue tracker (assuming you haven't already), and then speak to the maintainer about how you could help with the issue tracker. Often they will be able to help you get started.

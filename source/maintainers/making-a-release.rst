Making a Release
================

Releases are how maintainers tell the world—or, more accurately, downstream developers—that it's time to update their dependencies. These guidelines explain how maintainers can generate these releases.

Preliminaries
-------------

In the Git repository:

* Clean state: ``git status``.
* Up-to-date: ``git pull``.
* Ideally without untracked files: ``git clean`` can be handy but must be used **with caution** (for example ``git clean -xdf``).
* If signing commits/tags (which you should be), ensure that your up-to-date GPG key is associated with your Gitlab account - this can be checked in your `GitLab GPG key settings <https://gitlab.gnome.org/-/profile/gpg_keys>`_.

Tip: there's nothing preventing you from having two copies of the Git repo, with one only being used for releases.

For the Git commit
------------------

* Update the version number if necessary (in ``meson.build`` or equivalent).
   * For a library, update also the interface version (for the soname, see :ref:`library interface versions <library api versions>` below).
* Update the ``README`` if necessary.
* If the module is an application, add a ``<release>`` entry in the AppStream metadata file (refer to the `AppStream documentation <https://www.freedesktop.org/wiki/Software/>`_). You should read through the Git commit log and come up with a bullet point for each significant change and credit the person(s) who did the work. Some tips for doing this:
   * Remember that the AppStream metadata is presented by GUI tools to the user.
   * Try to make the bullet points short and snappy but also understandable for a general user (not always easy).
   * If several entries relate to the same change only use a single bullet point.
* **Note**: the AppStream specification does not deal with alphabetical components inside a version, so whenever releasing a development snapshot (``alpha``, ``beta``, or ``rc``) you will need to use tilde as a separator for the ``version`` attribute in the ``release`` element, e.g. ``43.alpha`` becomes ``43~alpha``; ``44.beta`` becomes ``44~beta``; and ``45.rc`` becomes ``45~rc``.
   * You also have the option to not list development snapshots in the AppStream metadata, and instead make a full release description for the first stable release.
* Update the ``NEWS`` file:
   * `A script is available <https://gitlab.gnome.org/pwithnall/gitlab-changelog/>`_ for generating a ``NEWS`` file from your Git history.
   * The ``NEWS`` file is generally used by packagers and integrators, so you can be more verbose than in the AppStream metadata.
   * Do not forget to include updated dependencies, since it makes life easier for packagers.
   * You might use a format similar to the following:

:: 

  Version 43.0
  ------------
  * Dependency update
  * Some change
  * A bug fix
  * Small maintenance tasks
  * Translation updates

* Do a ``git commit -S``

Rolling the tarball
-------------------

With Meson:

* Do a ``meson dist``
* Fix any issues that crop up.
* If you need to make additional changes to fix meson dist, don’t forget to re-commit.

If successful, it should show you something like this:

``Distribution package /opt/gnome/build/glib/meson-dist/glib-2.57.3.tar.xz tested.``

git tag and git push
--------------------

Create a **git tag** to easily locate the version later on in the Git history.

* Best way: ``git evtag sign 43.0`` with `git-evtag <https://github.com/cgwalters/git-evtag>`_, to provide strong signing guarantees.
* If you can’t do that, use the basic way: ``git tag -s 43.0``
* Or, worse: ``git tag -a 43.0``

The message of the tag should be in the format:

::

  GNOME Foo 43.0
  
  * The contents of the NEWS file for this release.
  * Dependency updates
  * Some change
  * A bug fix
  * Small maintenance tasks
  * Translation updates


Then **git push** the commit and tag: ``git push --atomic origin HEAD 43.0``
 * Push the commit as usual.
 * In the rare case where another commit was pushed in the meantime (usually for a translation), you can do a normal git pull (so **without** ``--rebase``) and there will be a merge commit that you can push (the translation will need to wait the next release).

Upload the tarball
------------------

::

  $ scp gnome-foo-43.0.tar.xz USER@master.gnome.org:~
  $ ssh USER@master.gnome.org
  $ ftpadmin install gnome-foo-43.0.tar.xz

(Adapt to your taste).

Notes:

* All module maintainers who wish to be able to upload tarballs should request a shell account at master.gnome.org for this purpose (see :doc:`accounts </infrastructure/accounts/managing-accounts>`). Ask someone else to do it for you if you are waiting for an account.
* There are no extra steps required for new modules.
* The ``ftpadmin install`` will move the tarball to the appropriate directory, do some additional administrative stuff, and email members of the Release Team so they know about it.

.. _library api versions:

Updating library interface versions
-----------------------------------

Note: this is relevant if you are still using Autotools, or have migrated from Autotools to Meson within the same major version. If you are writing a new library that has never used Autotools, or if you just bumped the API version and only ever used Meson, you can ignore all of it and use the ``soversion`` and ``version`` parameters of the `library method <https://mesonbuild.com/Reference-manual_functions.html#library>`_.

For the ``-version-info`` linker flag, which is used to create the ``soname`` of the library:

* There is usually a variable with a name such as ``LT_VERSION`` towards the top of the ``configure.ac``, ``meson.build`` or equivalent file.
* There is usually also a comment that summarizes how the version should be updated.

See the `Libtool manual <https://www.gnu.org/software/libtool/manual/html_node/Versioning.html>`_ for reference documentation on this, and the Programming Guidelines on `developer.gnome.org <https://developer.gnome.org/>`_.

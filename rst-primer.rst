reStructured Text Primer
========================

The GNOME handbook is formatted using `reStructuredText <https://docutils.sourceforge.io/rst.html>`_. You don't need to know much of it for small contributions, but here is the formatting that you do need to know.

Headings
--------

Headings have three levels:

:: 

  Page Title
  ==========

::

  Section header
  --------------

::

  Subsection header
  ~~~~~~~~~~~~~~~~~

Unordered lists
---------------

:: 

  * Item 1
  * Item 2
    * Item 2.1

Top level items don't have any indentation. Nested lists are indented using 2 spaces.

Ordered lists
-------------

::

  #. Item 1
  #. Item 2
    #. Item 2.1

Internal links
--------------

Internal page link format:

::

  :doc:`link text </path/to/page>`

For example:

::

  :doc:`issue review process </issues/review>`

External links
--------------

External web page link format:

::

  `link text <URL>`_

For example:

::

  `code of conduct <https://conduct.gnome.org/>`_

Inline formatting
-----------------

.. list-table::
  :widths: 50 50

  * - *Italics*
    - ``*Italics*``
  * - **Bold**
    - ``**Bold**``
  * - ``Monospace``
    - ````Monospace````
